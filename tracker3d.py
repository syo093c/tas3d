# when use binary MegEngine and Pytorch, it will set $CUDNN$.
# Put MegEngine at first to make it functional.
 
from detection3d import Detection3d
from CREStereo.test import load_model
from DepthEstimate import DepthEstimate
import cv2
from tqdm import tqdm
import os
import argparse

from DepthEstimate import DepthEstimate,load_model
from ovsd import Ovsd,show_box,show_mask
import cv2
import numpy as np
import matplotlib.pyplot as plt
from visualiz import Visualiz
from scipy.signal import medfilt
from scipy.spatial import ConvexHull
import open3d as o3d
from tqdm import tqdm
import glob
import sys
sys.path.append('./tracking/ByteTrack')
from tracking.ByteTrack.yolox.tracker.byte_tracker import BYTETracker
from visualiz import Visualiz
import numpy as np
from yolox.exp import get_exp
import pandas as pd


class Track3d():
    def __init__(self,detector:Detection3d,prompt='person.') -> None:
        self.detector = detector
        args={}
        #args["track_thresh"]=0.5
        #args["track_buffer"]=30
        #args["mot20"]=False
        #args["match_thresh"]=0.8

        args["track_thresh"]=0.6
        args["track_buffer"]=40
        args["mot20"]=False
        args["match_thresh"]=0.8 # lager matching is easier

        self.tracker = BYTETracker(args=args)
        self.prompt= prompt
    

def video_demo(t:Track3d, video_path):
    # delete cache
    cmd='rm -rfv ./video_tmp/*'
    os.system(cmd)
    cap=cv2.VideoCapture(video_path)
    width= cap.get(cv2.CAP_PROP_FRAME_WIDTH) # float
    height= cap.get(cv2.CAP_PROP_FRAME_HEIGHT) # float
    frame_number=int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    prompt=t.prompt
    
    v=Visualiz()
    v.init_canvas()
    export_results=[]
    for n in tqdm(range(frame_number)):
        ret, frame = cap.read()
        n+=1

        #print('Frame: '+str(n))

        if not ret:
        #if not ret or n==5:
            break
        
        # 2d image as input, get 3d information
        t.detector.update(frame,prompt=prompt)
        #t.detector.visualization_2d()
        bboxes_3d=t.detector.bboxes_3d
        #scores=t.detector.scores
        scores=np.ones(len(bboxes_3d))
        output_tracks=t.tracker.update(output_results=np.array(bboxes_3d),scores=scores)

        points_3d=t.detector.dpe.xyz
        rep_3d=[]
        for i in output_tracks:
            print(i.mean)
            print(i.mean[6])
            print(i.mean[7])
            dx=i.mean[6]
            dz=i.mean[8]
            dv=(dx**2+dz**2)**0.5
            if dz != 0:
                angle=np.arctan([dz/dx])
                print(angle)
            else:
                angle=0
                print(0)
            #rep_3d.append(t.detector.generate_person_template(i.cexyz[:3],i.cexyz[3:6],rotation=angle))
            rep_3d.append(np.array([*i.cexyz[0:3],*i.cexyz[3:6],0]))
        #bboxes_3d=[t.detector.generate_person_template(i.cexyz[:3],i.cexyz[3:6],rotation=angle) for i in output_tracks]
        for i in output_tracks:
            export_results.append(np.append([n,i.track_id],i.mean))

        labels=[j.track_id for i,j in enumerate(output_tracks)]
        image = cv2.cvtColor(t.detector.dpe.img_left,cv2.COLOR_BGR2RGB)/255.
        point_colors=image.reshape(-1,3)

        # just visualize it with Open3D!!
        img=v.update(points=points_3d,
                point_colors=point_colors,
                ref_boxes=np.array(rep_3d),
                ref_labels=labels)
        
        cv2.imwrite('./video_tmp/'+str(n).zfill(6)+'.png',img)
    cmd='ffmpeg -f image2 -i ./video_tmp/%06d.png -c:v h264_nvenc -qp 0 '+'3d_detection_result_'+os.path.basename(opt.input)
    os.system(cmd) 
    
    #export csv
    export_results=pd.DataFrame(export_results,columns=['frame_id','track_id','x','y','z','a','b','ey','vx','vy','vz','va','vb','vl'])
    export_results.to_csv('data.csv')        


def video(opt):
    ovsd=Ovsd()
    crestereo_model=load_model('./CREStereo/crestereo_eth3d.mge')
    dpe=DepthEstimate(model=crestereo_model,need_to_calibrate=1)
    det3d=Detection3d(dpe=dpe,ovsd=ovsd)

    #prompt='hand.wallet.laptop.headphone.'
    #prompt='wallet.laptop.doll.keyboard.cellphone.bottle.'
    #prompt='doll.cellphone.bottle.keyboard.laptop.'
    #prompt='person.white board.desk'
    prompt='trashbin'
    my_3d_tracker=Track3d(detector=det3d,prompt=prompt)
    video_demo(my_3d_tracker,opt.input)


def debug(opt):
    ovsd=Ovsd()
    crestereo_model=load_model('./CREStereo/crestereo_eth3d.mge')
    dpe=DepthEstimate(model=crestereo_model,need_to_calibrate=1)
    det3d=Detection3d(dpe=dpe,ovsd=ovsd)

    mytracker=Track3d(det3d)
    
    cap=cv2.VideoCapture(opt.input)
    width= cap.get(cv2.CAP_PROP_FRAME_WIDTH) # float
    height= cap.get(cv2.CAP_PROP_FRAME_HEIGHT) # float
    frame_number=int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    
    v=Visualiz()
    v.init_canvas()
    export_results=[]
    for n in tqdm(range(frame_number)):
        ret, frame = cap.read()
        n += 1
        if not ret:
            break
        img = frame
        result = det2d_model.detect_human(img)
        img = det2d_model.visual(img=img, result=result)
        cv2.imwrite('./video_tmp/'+str(n).zfill(6)+'.png', img)
    cmd = 'ffmpeg -f image2 -i ./video_tmp/%06d.png -c:v h264_nvenc -qp 0 ' + '2d_detection_result_'+os.path.basename(opt.input)
    os.system(cmd)

if __name__ == "__main__":
    arg=argparse.ArgumentParser()
    arg.add_argument('function',default='video')
    arg.add_argument('--input','-i',default='test1.mp4')
    opt=arg.parse_args()
    if opt.function == 'video':
        video(opt)
    if opt.function == 'debug':
        debug(opt)
