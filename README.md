# tas3d


This is a library for 3D person detection, tracking, and behavior analysis based on stereo cameras. This library is licensed under MIT license.
A library for ```Open Vocabulary 3D mutliple object tracking``` based on 

You can use this library to:
1. Calibrate stereo camera (generate a ```pkl``` calibration file).
2. Perform depth estimation using stereo matching.
   Supports multiple algorithms.
   
   2.1 OpenCV block matching
   
   2.2 Google HitNet
   
   2.3 CREStereo (recommended)

3. Detect of any object indicated by natural language.
4. Track object in 3D space and obtain thier 3D point cloud.
   
### Install
```
cd ./tracking/cython_bbox_3d
pip install -e .
```

### Use
```
DepthEstimate.py              ->    Depth estimate 
Detection2d.py                ->    Open Vocabulary Detection and Segmentation in 2D 
Detection3d.py                ->    Open Vocabulary Detection in 3D lefting from 2D
Track3d.py                    ->    Tracking in 3D space
StereoCalibration.py          ->    Calibration
```

### Todo
1. Improve stereo matching performance using super-resolution methods.
2. perform detection and tracking using End-to-End method.


![trakcing1](https://gitlab.com/syo093c/tas3d/-/raw/main/media/puipui.gif)
![tracking2](https://gitlab.com/syo093c/tas3d/-/raw/main/media/trashbin.gif)
![tracking3](https://gitlab.com/syo093c/tas3d/-/raw/main/media/s2.gif)
