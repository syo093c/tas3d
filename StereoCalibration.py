# some code is borrowed from https://github.com/bvnayak/stereo_calibration.git
import numpy as np
import cv2 as cv
import glob
import argparse
import pickle
import os
from tqdm import tqdm
import random


class StereoCalibration(object):
    def __init__(self, filepath):
        # termination criteria
        self.criteria = (cv.TERM_CRITERIA_EPS +
                         cv.TERM_CRITERIA_MAX_ITER, 30, 0.001)
        self.criteria_cal = (cv.TERM_CRITERIA_EPS +
                             cv.TERM_CRITERIA_MAX_ITER, 100, 1e-5)

        self.square_size=0.026
        self.chess_board_size=(14,9)
        # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
        self.objp = np.zeros((self.chess_board_size[0]*self.chess_board_size[1], 3), np.float32)
        self.objp[:, :2] = np.mgrid[0:self.chess_board_size[0], 0:self.chess_board_size[1]].T.reshape(-1, 2)
        #print(self.objp)
        self.objp *= self.square_size
        #print(self.objp)
        #self.objp = np.zeros((9*6, 3), np.float32)
        #self.objp[:, :2] = np.mgrid[0:14, 0:9].T.reshape(-1, 2)

        # Arrays to store object points and image points from all the images.
        self.objpoints = []  # 3d point in real world space
        self.imgpoints_l = []  # 2d points in image plane.
        self.imgpoints_r = []  # 2d points in image plane.

        self.cal_path = filepath
        self.read_images(self.cal_path)

    def read_images(self, cal_path):
        images=glob.glob(cal_path+'*.png')
        #num_iterms=10
        #images = random.sample(images, num_iterms)
        images.sort()

        for i, fname in enumerate(images):
            img=cv.imread(images[i])
            img_l=img[:img.shape[0],:int(img.shape[1]/2)]
            img_r=img[:img.shape[0],int(img.shape[1]/2):]

            gray_l = cv.cvtColor(img_l, cv.COLOR_BGR2GRAY)
            gray_r = cv.cvtColor(img_r, cv.COLOR_BGR2GRAY)
            #print(i)
            # Find the chess board corners
            flags = cv.CALIB_CB_NORMALIZE_IMAGE | cv.CALIB_CB_EXHAUSTIVE | cv.CALIB_CB_ACCURACY
            ret_l, corners_l = cv.findChessboardCornersSB(gray_l, (self.chess_board_size[0], self.chess_board_size[1]), flags)
            ret_r, corners_r = cv.findChessboardCornersSB(gray_r, (self.chess_board_size[0],self.chess_board_size[1]), flags)

            print(i)
            if ret_l is True and ret_r is True:
                print('found')
                # If found, add object points, image points (after refining them)
                self.objpoints.append(self.objp)

                #rt = cv.cornerSubPix(gray_l, corners_l, (11, 11), (-1, -1), self.criteria)
                self.imgpoints_l.append(corners_l)

                # Draw and display the corners
                ret_l = cv.drawChessboardCorners(img_l, (self.chess_board_size[0], self.chess_board_size[1]),
                                                  corners_l, ret_l)
                cv.imwrite('l'+str(i)+'.jpg',img_l)

                #rt = cv.cornerSubPix(gray_r, corners_r, (11, 11), (-1, -1), self.criteria)
                self.imgpoints_r.append(corners_r)

                # Draw and display the corners
                ret_r = cv.drawChessboardCorners(img_r, (self.chess_board_size[0],self.chess_board_size[1]),
                                                  corners_r, ret_r)
                cv.imwrite('r'+str(i)+'.jpg',img_r)
            img_shape = gray_l.shape[::-1]


        rt, self.M1, self.d1, self.r1, self.t1 = cv.calibrateCamera(
            self.objpoints, self.imgpoints_l, img_shape, None, None)
        rt, self.M2, self.d2, self.r2, self.t2 = cv.calibrateCamera(
            self.objpoints, self.imgpoints_r, img_shape, None, None)
        self.camera_model = self.stereo_calibrate(img_shape)

    def stereo_calibrate(self, dims):
        flags = 0
        flags |= cv.CALIB_FIX_INTRINSIC
        # flags |= cv.CALIB_FIX_PRINCIPAL_POINT
        flags |= cv.CALIB_USE_INTRINSIC_GUESS
        flags |= cv.CALIB_FIX_FOCAL_LENGTH
        # flags |= cv.CALIB_FIX_ASPECT_RATIO
        flags |= cv.CALIB_ZERO_TANGENT_DIST
        # flags |= cv.CALIB_RATIONAL_MODEL
        # flags |= cv.CALIB_SAME_FOCAL_LENGTH
        # flags |= cv.CALIB_FIX_K3
        # flags |= cv.CALIB_FIX_K4
        # flags |= cv.CALIB_FIX_K5

        stereocalib_criteria = (cv.TERM_CRITERIA_MAX_ITER +
                                cv.TERM_CRITERIA_EPS, 100, 1e-5)
        print(len(self.objpoints))
        ret, M1, d1, M2, d2, R, T, E, F = cv.stereoCalibrate(
            self.objpoints, self.imgpoints_l,
            self.imgpoints_r, self.M1, self.d1, self.M2,
            self.d2, dims,
            criteria=stereocalib_criteria, flags=flags)

        print('Intrinsic_mtx_1', M1)
        print('dist_1', d1)
        print('Intrinsic_mtx_2', M2)
        print('dist_2', d2)
        print('R', R)
        print('T', T)
        print('E', E)
        print('F', F)

        # for i in range(len(self.r1)):
        #     print("--- pose[", i+1, "] ---")
        #     self.ext1, _ = cv.Rodrigues(self.r1[i])
        #     self.ext2, _ = cv.Rodrigues(self.r2[i])
        #     print('Ext1', self.ext1)
        #     print('Ext2', self.ext2)

        print('')

        camera_model = dict([('M1', M1), ('M2', M2), ('dist1', d1),
                            ('dist2', d2), ('rvecs1', self.r1),
                            ('rvecs2', self.r2), ('R', R), ('T', T),
                            ('E', E), ('F', F)])
        
        with open('stereo_camera_calibration_result.pkl', 'wb') as f:
            pickle.dump(camera_model, f)

        #cv.destroyAllWindows()
        return camera_model


def generate_calibrated_video(video_path,output):
    with open('./stereo_camera_calibration_result.pkl','rb') as f:
        camera_parameter=pickle.load(f)
    cap = cv.VideoCapture(video_path)
    n=0
    frame_number=int(cap.get(cv.CAP_PROP_FRAME_COUNT))
    print(frame_number)
    if os.path.exists('tmp'):
        os.system('rm -rf ./tmp')
    os.mkdir('tmp')

    for n in tqdm(range(frame_number)):
        ret,frame=cap.read()
        if not ret:
            break
        imgL=frame[:frame.shape[0],:int(frame.shape[1]/2)]
        imgR=frame[:frame.shape[0],int(frame.shape[1]/2):]
        
        m1=camera_parameter['M1']
        dist1=camera_parameter['dist1']
        m2=camera_parameter['M2']
        dist2=camera_parameter['dist2']
        h,  w = imgL.shape[:2]
        size=(h,w)
        r=camera_parameter['R']
        t=camera_parameter['T']

        R1, R2, P1, P2, Q, roi1, roi2= cv.stereoRectify(m1,dist1,m2,dist2,size,r,t)
        mapx, mapy = cv.initUndistortRectifyMap(m1, dist1,R1, P1, (w,h), 5)
        dst_l = cv.remap(imgL, mapx, mapy, cv.INTER_LINEAR)
        mapx, mapy = cv.initUndistortRectifyMap(m2, dist2, R2, P2, (w,h), 5)
        dst_r = cv.remap(imgR, mapx, mapy, cv.INTER_LINEAR)

        cv.imwrite('tmp/'+str(n).zfill(6)+'.png',np.concatenate((dst_l,dst_r),axis=1))
        
    cmdl='ffmpeg -f image2 -i ./tmp/%06d.png -c:v h264_nvenc -qp 0 '+ output
    os.system(cmdl)
    os.system('rm -rvf ./tmp')

def print_camera_parametar():
    with open('./stereo_camera_calibration_result.pkl','rb') as f:
        camera_parameter=pickle.load(f)
    print(camera_parameter)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--filepath', '-p', help='String Filepath', default='./dataset/png/')
    args = parser.parse_args()
    cal_data = StereoCalibration(args.filepath)
