import argparse
import os
import copy

import numpy as np
import json
import torch
from PIL import Image, ImageDraw, ImageFont

# Grounding DINO
import groundingdino.datasets.transforms as T
from groundingdino.models import build_model
from groundingdino.util import box_ops
from groundingdino.util.slconfig import SLConfig
from groundingdino.util.utils import clean_state_dict, get_phrases_from_posmap

# segment anything
from segment_anything import (
    sam_model_registry,
    build_sam_hq,
    SamPredictor
)
import cv2
import numpy as np
import matplotlib.pyplot as plt

def load_groundingDINO(model_config_path, model_checkpoint_path, device):
    args = SLConfig.fromfile(model_config_path)
    args.device = device
    model = build_model(args)
    checkpoint = torch.load(model_checkpoint_path, map_location="cpu")
    load_res = model.load_state_dict(clean_state_dict(checkpoint["model"]), strict=False)
    print(load_res)
    _ = model.eval()
    return model



def groudingDINO_preprocess(img:cv2.Mat):
    # load image
    #image_pil = Image.open(image_path).convert("RGB")  # load image
    image_pil = Image.fromarray(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))


    transform = T.Compose(
        [
            T.RandomResize([800], max_size=1333),
            T.ToTensor(),
            T.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]),
        ]
    )
    image, _ = transform(image_pil, None)  # 3, h, w
    return image_pil, image

class Ovsd():
    """
    2d open vocabulary detection and segmentation
    """
    
    def __init__(self) -> None:
        self.ovd=load_groundingDINO("Grounded-Segment-Anything/GroundingDINO/groundingdino/config/GroundingDINO_SwinT_OGC.py", \
                                    "Grounded-Segment-Anything/weights/groundingdino_swint_ogc.pth",\
                                    "cuda")
        self.sam=SamPredictor(build_sam_hq(checkpoint="Grounded-Segment-Anything/weights/sam_hq_vit_h.pth").to("cuda"))
        #self.sam=SamPredictor(build_sam_hq(checkpoint="Grounded-Segment-Anything/weights/sam_hq_vit_b.pth").to("cuda"))
        self.sam:SamPredictor

    def _get_grounding_output(self, image, caption, box_threshold, text_threshold, with_logits=True, device="cuda"):
        caption = caption.lower()
        caption = caption.strip()
        if not caption.endswith("."):
            caption = caption + "."
        model = self.ovd.to(device)
        image = image.to(device)
        with torch.no_grad():
            outputs = model(image[None], captions=[caption])
        logits = outputs["pred_logits"].cpu().sigmoid()[0]  # (nq, 256)
        boxes = outputs["pred_boxes"].cpu()[0]  # (nq, 4)
        logits.shape[0]

        # filter output
        logits_filt = logits.clone()
        boxes_filt = boxes.clone()
        filt_mask = logits_filt.max(dim=1)[0] > box_threshold
        logits_filt = logits_filt[filt_mask]  # num_filt, 256
        boxes_filt = boxes_filt[filt_mask]  # num_filt, 4
        logits_filt.shape[0]

        # get phrase
        tokenlizer = model.tokenizer
        tokenized = tokenlizer(caption)
        # build pred
        pred_phrases = []
        for logit, box in zip(logits_filt, boxes_filt):
            pred_phrase = get_phrases_from_posmap(logit > text_threshold, tokenized, tokenlizer)
            if with_logits:
                pred_phrases.append(pred_phrase + f"({str(logit.max().item())[:4]})")
            else:
                pred_phrases.append(pred_phrase)

        return boxes_filt, pred_phrases
    
    def update(self,img,text_prompt):
        image_pil,image=groudingDINO_preprocess(img)

        boxes_filt, pred_phrases = self._get_grounding_output(
            image, text_prompt, 0.3, 0.25, device="cuda"
        )
        image = img
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        self.sam.set_image(img)

        size = image_pil.size
        H, W = size[1], size[0]
        for i in range(boxes_filt.size(0)):
            boxes_filt[i] = boxes_filt[i] * torch.Tensor([W, H, W, H])
            boxes_filt[i][:2] -= boxes_filt[i][2:] / 2
            boxes_filt[i][2:] += boxes_filt[i][:2]

        boxes_filt = boxes_filt.cpu()
        transformed_boxes = self.sam.transform.apply_boxes_torch(boxes_filt, image.shape[:2]).to("cuda")

        masks, _, _ = self.sam.predict_torch(
            point_coords = None,
            point_labels = None,
            boxes = transformed_boxes.to("cuda"),
            multimask_output = False,
        )
        
        return boxes_filt, pred_phrases, masks

def show_mask(mask, ax, random_color=False):
    if random_color:
        color = np.concatenate([np.random.random(3), np.array([0.6])], axis=0)
    else:
        color = np.array([30/255, 144/255, 255/255, 0.6])
    h, w = mask.shape[-2:]
    mask_image = mask.reshape(h, w, 1) * color.reshape(1, 1, -1)
    ax.imshow(mask_image)

def show_box(box, ax, label):
    x0, y0 = box[0], box[1]
    w, h = box[2] - box[0], box[3] - box[1]
    ax.add_patch(plt.Rectangle((x0, y0), w, h, edgecolor='green', facecolor=(0,0,0,0), lw=2)) 
    ax.text(x0, y0, label)

def main():
    ovsd=Ovsd()
    img=cv2.imread('./test.jpg')
    prompt="whiteboard"
    boxes_filt,pred_phrases,masks=ovsd.update(img,prompt)

    plt.figure(figsize=(10, 10))
    plt.imshow(img)
    for mask in masks:
        show_mask(mask.cpu().numpy(), plt.gca(), random_color=True)
    for box, label in zip(boxes_filt, pred_phrases):
        show_box(box.numpy(), plt.gca(), label)
    plt.savefig(
        "grounded_sam_output.jpg", 
        bbox_inches="tight", dpi=300, pad_inches=0.0
    )

if __name__ == '__main__':
    main()
