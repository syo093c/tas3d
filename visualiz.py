import open3d
import torch
import matplotlib
import numpy as np
import cv2 as cv
import sys
import os
#np.set_printoptions(threshold=sys.maxsize)

class Visualiz():
    """
    A Non-blocking 3d visualizer to draw bboxes, text.
    http://www.open3d.org/docs/release/tutorial/visualization/non_blocking_visualization.html
    
    """
    def __init__(self) -> None:
        COLORS_10 = [(144, 238, 144), (178, 34, 34), (221, 160, 221), (0, 255, 0), (0, 128, 0), (210, 105, 30), (220, 20, 60),
             (192, 192, 192), (255, 228, 196), (50, 205, 50), (139, 0, 139), (100, 149, 237), (138, 43, 226),
             (238, 130, 238),
             (255, 0, 255), (0, 100, 0), (127, 255, 0), (255, 0, 255), (0, 0, 205), (255, 140, 0), (255, 239, 213),
             (199, 21, 133), (124, 252, 0), (147, 112, 219), (106, 90, 205), (176, 196, 222), (65, 105, 225),
             (173, 255, 47),
             (255, 20, 147), (219, 112, 147), (186, 85, 211), (199, 21, 133), (148, 0, 211), (255, 99, 71),
             (144, 238, 144),
             (255, 255, 0), (230, 230, 250), (0, 0, 255), (128, 128, 0), (189, 183, 107), (255, 255, 224),
             (128, 128, 128),
             (105, 105, 105), (64, 224, 208), (205, 133, 63), (0, 128, 128), (72, 209, 204), (139, 69, 19),
             (255, 245, 238),
             (250, 240, 230), (152, 251, 152), (0, 255, 255), (135, 206, 235), (0, 191, 255), (176, 224, 230),
             (0, 250, 154),
             (245, 255, 250), (240, 230, 140), (245, 222, 179), (0, 139, 139), (143, 188, 143), (255, 0, 0),
             (240, 128, 128),
             (102, 205, 170), (60, 179, 113), (46, 139, 87), (165, 42, 42), (178, 34, 34), (175, 238, 238),
             (255, 248, 220),
             (218, 165, 32), (255, 250, 240), (253, 245, 230), (244, 164, 96), (210, 105, 30)]

        self.box_colormap = np.array(COLORS_10)/255
        self.geometry_list=[]
        self.canvas_inited=False

    def init_canvas(self):
        self.canvas_inited=True
        vis = open3d.visualization.Visualizer()
        vis.create_window()
        vis.get_render_option().point_size = 1.0
        vis.get_render_option().background_color = np.zeros(3)
        # arrange view
        view_ctl = vis.get_view_control()
        #view_ctl.set_up((1, 0, 0))  # set the positive direction of the x-axis as the up direction
        view_ctl.set_up((0, -1, 0))  # set the negative direction of the y-axis as the up direction
        view_ctl.set_front((0, 0, -1))  # set the positive direction of the x-axis toward you
        view_ctl.set_lookat((0, 0, 0))  # set the original point as the center point of the window
        view_ctl.rotate(60.0, 30.0)
        view_ctl.set_zoom(0.2)
        self.vis=vis
        self.flag=1
        
    def destroy(self):
        self.vis.destroy_window()
    def set_view(self):
        view_ctl = self.vis.get_view_control()
        #view_ctl.set_up((1, 0, 0))  # set the positive direction of the x-axis as the up direction
        view_ctl.set_up((0, -1, 0))  # set the negative direction of the y-axis as the up direction
        view_ctl.set_front((0, 0, -1))  # set the positive direction of the x-axis toward you
        view_ctl.set_lookat((0, 0, 0))  # set the original point as the center point of the window
        view_ctl.rotate(60.0, 30.0)
        view_ctl.set_zoom(0.05)

    def get_coor_colors(self,obj_labels):
        """
        Args:
            obj_labels: 1 is ground, labels > 1 indicates different instance cluster

        Returns:
            rgb: [N, 3]. color for each point.
        """
        colors = matplotlib.colors.XKCD_COLORS.values()
        max_color_num = obj_labels.max()

        color_list = list(colors)[:max_color_num+1]
        colors_rgba = [matplotlib.colors.to_rgba_array(color) for color in color_list]
        label_rgba = np.array(colors_rgba)[obj_labels]
        label_rgba = label_rgba.squeeze()[:, :3]

        return label_rgba


    def update(self,points=None, gt_boxes=None, ref_boxes=None, ref_labels=None, 
                ref_scores=None, point_colors=None, draw_origin=False,skeletons=None):
        # first remove all old geometry
        self.flag-=1
        #for i in self.geometry_list:
        #    self.vis.remove_geometry(i,reset_bounding_box=(self.flag==0))
        self.vis.clear_geometries()
        self.geometry_list=[]

        if points is not None:
            #add new geometry to list
            if isinstance(points, torch.Tensor):
                points = points.cpu().numpy()
            if isinstance(gt_boxes, torch.Tensor):
                gt_boxes = gt_boxes.cpu().numpy()
            if isinstance(ref_boxes, torch.Tensor):
                ref_boxes = ref_boxes.cpu().numpy()

            # draw origin
            if draw_origin:
                axis_pcd = open3d.geometry.TriangleMesh.create_coordinate_frame(size=1.0, origin=[0, 0, 0])
                self.geometry_list.append(axis_pcd)

            pts = open3d.geometry.PointCloud()
            pts.points = open3d.utility.Vector3dVector(points[:, :3])

            self.geometry_list.append(pts)
            if point_colors is None:
                pts.colors = open3d.utility.Vector3dVector(np.ones((points.shape[0], 3)))
                #pass
            else:
                pts.colors = open3d.utility.Vector3dVector(point_colors)

        if gt_boxes is not None:
            for i in self.draw_box(ref_boxes, (0, 0, 1), ref_labels, ref_scores):
                self.geometry_list.append(i)

        if ref_boxes is not None:
            for i in self.draw_box(ref_boxes, (0, 1, 0), ref_labels, ref_scores):
                self.geometry_list.append(i)
        
        if skeletons is not None:
            for i in self.draw_skeleton(skeletons):
                self.geometry_list.append(i)

        #show all geometry in list
        for i in self.geometry_list:
            self.vis.add_geometry(i,reset_bounding_box=(self.flag==0))

        #self.vis.reset_view_point(1)
        if self.flag==0:
            self.set_view()
        self.vis.poll_events()
        self.vis.update_renderer()

        img=self.vis.capture_screen_float_buffer(do_render=True)
        """
            OMG, class `Image` in open3d use (0-1) and rgb as the format of
            pints
            But, Opencv use (0-255) and gbr as the format of points

            so, if you want to save a video using opencv, don't forget
            format convertion.
            Note that, cv.imshow will judge format by type, float -> (0,1),
            int -> (0-255), f*ck!
        """
        img = np.array(img)*255
        img=np.uint8(cv.cvtColor(img,cv.COLOR_RGB2BGR))

        # let's play with the results
        self.vis.run()

        return img
        #vis.destroy_window()


    def translate_boxes_to_open3d_instance(self,gt_boxes):
        """
                 4-------- 6
               /|         /|
              5 -------- 3 .
              | |        | |
              . 7 -------- 1
              |/         |/
              2 -------- 0
        """
        center = gt_boxes[0:3]
        lwh = gt_boxes[3:6] # open3d.geometry.OrientedBoundingBox, create from center
        # rotation, extended in x, y, z direction.
        #axis_angles = np.array([0, 0, gt_boxes[6] + 1e-10])
        axis_angles = np.array([0,  gt_boxes[6] + 1e-10,0])
        rot = open3d.geometry.get_rotation_matrix_from_axis_angle(axis_angles)
        box3d = open3d.geometry.OrientedBoundingBox(center, rot, lwh)

        line_set = open3d.geometry.LineSet.create_from_oriented_bounding_box(box3d)

        # import ipdb; ipdb.set_trace(context=20)
        lines = np.asarray(line_set.lines)
        #lines = np.concatenate([lines, np.array([[1, 4], [7, 6]])], axis=0)

        line_set.lines = open3d.utility.Vector2iVector(lines)

        return line_set, box3d

    def translate_skeleton_to_open3d_instance(self,skeleton):
        # COCO dataset
        line_sk = [[15, 13], [13, 11], [16, 14], [14, 12], [11, 12],
            [5, 11], [6, 12], [5, 6], [5, 7], [6, 8], [7, 9],
            [8, 10], [1, 2], [0, 1], [0, 2], [1, 3], [2, 4],
            [3, 5], [4, 6]]
        colors = [[1, 0, 0] for i in range(len(line_sk))]
        line_set = open3d.geometry.LineSet(
            points=open3d.utility.Vector3dVector(skeleton),
            lines=open3d.utility.Vector2iVector(line_sk),
        )
        line_set.colors = open3d.utility.Vector3dVector(colors)

        return line_set

    def draw_box(self, gt_boxes, color=(0, 1, 0), ref_labels=None, score=None):
        box_list=[]
        for i in range(gt_boxes.shape[0]):
            line_set, box3d = self.translate_boxes_to_open3d_instance(gt_boxes[i])
            if ref_labels is None:
                line_set.paint_uniform_color(color)
            else:
                line_set.paint_uniform_color(self.box_colormap[ref_labels[i]%len(self.box_colormap)])

            box_list.append(line_set)
        return box_list
    
    def draw_skeleton(self, skeletons,color=(1,1,0)):
        skeleton_list=[]
        for skeleton in skeletons:
            line_set=self.translate_skeleton_to_open3d_instance(skeleton)
            skeleton_list.append(line_set)
        return skeleton_list
