from tqdm import tqdm
import argparse
import copy
import cv2
import matplotlib.pyplot as plt
import megengine as mge
import megengine.functional as F
import numpy as np
import open3d as o3d
import os
import pickle
import sys
import tensorflow as tf
sys.path.append('./CREStereo')
from CREStereo.nets import Model 
from CREStereo.test import load_model
from visualiz import Visualiz

class DepthEstimate():
    def __init__(self,model='',need_to_calibrate=1,size=None,min_disp=None):
        with open('./stereo_camera_calibration_result.pkl','rb') as f:
            self.camera_parameter=pickle.load(f)
        self.model=model
        self.need_to_calibrate=need_to_calibrate
        self.size=size
        self.min_disp=min_disp
    
    def update(self,img):
        self.img=self.load_img(img)
        self._width=self.img_left.shape[1]
        self._height=self.img_left.shape[0]

    def cre_stereo(self):
        n_iter=20
        left=self.img_left
        right=self.img_right
        in_h, in_w = left.shape[:2]

        # add setting
        if self.size ==None:
            eval_h, eval_w = in_h,in_w
        else:
            eval_h, eval_w = self.size
        left = cv2.resize(left, (eval_w, eval_h), interpolation=cv2.INTER_LINEAR)
        right = cv2.resize(right, (eval_w, eval_h), interpolation=cv2.INTER_LINEAR)

        imgL = left.transpose(2, 0, 1)
        imgR = right.transpose(2, 0, 1)
        imgL = np.ascontiguousarray(imgL[None, :, :, :])
        imgR = np.ascontiguousarray(imgR[None, :, :, :])

        imgL = mge.tensor(imgL).astype("float32")
        imgR = mge.tensor(imgR).astype("float32")

        imgL_dw2 = F.nn.interpolate(
            imgL,
            size=(imgL.shape[2] // 2, imgL.shape[3] // 2),
            mode="bilinear",
            align_corners=True,
        )
        imgR_dw2 = F.nn.interpolate(
            imgR,
            size=(imgL.shape[2] // 2, imgL.shape[3] // 2),
            mode="bilinear",
            align_corners=True,
        )
        pred_flow_dw2 = self.model(imgL_dw2, imgR_dw2, iters=n_iter, flow_init=None)

        pred_flow = self.model(imgL, imgR, iters=n_iter, flow_init=pred_flow_dw2)
        pred_disp = F.squeeze(pred_flow[:, 0, :, :]).numpy()

        t = float(in_w) / float(eval_w)
        pred_disp = cv2.resize(pred_disp, (in_w, in_h), interpolation=cv2.INTER_LINEAR) * t
        
        if self.min_disp != None:
            pred_disp[pred_disp<self.min_disp]=self.min_disp

        #disp=pred_disp
        #disp_vis = (disp - disp.min()) / (disp.max() - disp.min()) * 255.0
        #disp_vis = disp_vis.astype("uint8")
        #disp_vis = cv2.applyColorMap(disp_vis, cv2.COLORMAP_INFERNO)
        #cv2.imwrite('dis.png', disp_vis)
        return pred_disp



    # Traditional method borrow from undergraduate students
    # https://github.com/pubgeo/dfc2019/blob/master/track2/test-sgbm.py
    def sgbm(self):
        # run SGM stereo matching with weighted least squares filtering
        #print('Running SGBM stereo matcher...')
        if len(self.img_left.shape) > 2:
            rimg1 = cv2.cvtColor(self.img_left, cv2.COLOR_BGR2GRAY)
            rimg2 = cv2.cvtColor(self.img_right, cv2.COLOR_BGR2GRAY)
        window_size = 5
        # Max disparity determind how close you can measure.
        left_matcher = cv2.StereoSGBM_create(
            #minDisparity=12,   # 10m
            #numDisparities=400,# 0.3m
            minDisparity=6,     # 20m
            numDisparities=128, # 1m
            blockSize=window_size,
            #blockSize=13,
            P1=8 * 3 * window_size ** 2,
            P2=32 * 3 * window_size ** 2,
            disp12MaxDiff=1,
            uniquenessRatio=15,
            #speckleWindowSize=500,
            speckleWindowSize=50,
            speckleRange=2,
            preFilterCap=63,
            mode=cv2.STEREO_SGBM_MODE_SGBM_3WAY
        )
        right_matcher = cv2.ximgproc.createRightMatcher(left_matcher)
        lmbda = 8000
        sigma = 1.5
        wls_filter = cv2.ximgproc.createDisparityWLSFilter(matcher_left=left_matcher)
        wls_filter.setLambda(lmbda)
        wls_filter.setSigmaColor(sigma)
        displ = left_matcher.compute(rimg1, rimg2)
        dispr = right_matcher.compute(rimg2, rimg1)
        displ = np.int16(displ)
        dispr = np.int16(dispr)
        disparity = wls_filter.filter(displ, rimg1, None, dispr) / 16.0
        disparity[np.where(disparity<0)] = 0
        disparity=disparity.astype('float32')
        self.disparity_map=disparity
        return disparity


    def _estimate_depth(self):
        #self.disparity_map=self.model(self.img_left,self.img_right)
        self.disparity_map=self.cre_stereo()
        #self.disparity_map=self.sgbm()

        # set max deepth
        #self.disparity_map= np.where(self.disparity_map>12,self.disparity_map,12)
        self.disparity_map= np.where(self.disparity_map>1,self.disparity_map,1)

    def load_img(self,img):
        #img=cv.resize(img,(img.shape[1],img.shape[0]),interpolation= cv.INTER_LINEAR)
        #img=cv.resize(img,(img.shape[1]//2,img.shape[0]//2),interpolation= cv.INTER_LINEAR)
        imgL=img[:img.shape[0],:int(img.shape[1]/2)]
        imgR=img[:img.shape[0],int(img.shape[1]/2):]
        
        m1=self.camera_parameter['M1']
        dist1=self.camera_parameter['dist1']
        m2=self.camera_parameter['M2']
        dist2=self.camera_parameter['dist2']
        h,  w = imgL.shape[:2]
        size=(h,w)
        r=self.camera_parameter['R']
        t=self.camera_parameter['T']

        R1, R2, P1, P2, Q, roi_l, roi_r= cv2.stereoRectify(m1,dist1,m2,dist2,size,r,t)
        mapx, mapy = cv2.initUndistortRectifyMap(m1, dist1,R1, P1, (w,h), 5)
        dst_l = cv2.remap(imgL, mapx, mapy, cv2.INTER_LINEAR)
        mapx, mapy = cv2.initUndistortRectifyMap(m2, dist2, R2, P2, (w,h), 5)
        dst_r = cv2.remap(imgR, mapx, mapy, cv2.INTER_LINEAR)

        if self.need_to_calibrate==0:
            #print('the image has alread been calibrated')
            #We have all readly generate calibrated video, so that
            #do not need to remap image again.
            #And Openpose will generate keypoint for calibrated video
            self.img_left=imgL
            self.img_right=imgR
        else:
            #print('calibrate input image')
            self.img_left=dst_l
            self.img_right=dst_r
        self.Q=Q

        self.c_x = -self.Q[0][3]
        self.c_y = -self.Q[1][3]
        self.f = self.Q[2][3]
        self.c_xt = self.c_x
        self.T_x = 1/self.Q[3][2]  # baseline
        #self.Q = np.array([
        #    [1, 0, 0, -self.c_x],
        #    [0, 1, 0, -self.c_y],
        #    [0, 0, 0, self.f],
        #    [0, 0, 1/0.12, 0]
        #])
        self._estimate_depth()
        self._estimate_3d()
        
    def _estimate_3d(self):
        #print(self.Q)
        #self.Q = np.array([
        #    [1, 0, 0, -620],
        #    [0, 1, 0, -188],
        #    [0, 0, 0, 721],
        #    [0, 0, 1/0.54, 0]
        #])
        self.img_3d=cv2.reprojectImageTo3D(disparity=self.disparity_map,Q=self.Q) 
        
        #points(xyz) for open3d format
        self.xyz = np.zeros((self.img_3d.shape[0]*self.img_3d.shape[1], 3))
        x=np.reshape(self.img_3d[:,:,0],-1)
        y=np.reshape(self.img_3d[:,:,1],-1)
        z=np.reshape(self.img_3d[:,:,2],-1)
        self.xyz[:,0]=x
        self.xyz[:,1]=y
        self.xyz[:,2]=z

    def to_camera_coordinate(self,pixel_point):
        # deepth, (x,y) to (X,Y,Z)
        #x*deepth/self.f
        return self.img_3d[pixel_point[0],pixel_point[1]]


    def plot_disparity_map(self):
        self.color_disparity = draw_disparity(self.disparity_map)
        cobined_image = np.hstack((self.img_left, self.img_right, self.color_disparity))
        cv2.namedWindow("Estimated disparity", cv2.WINDOW_NORMAL)	
        cv2.imshow("Estimated disparity", self.color_disparity)
        cv2.waitKey(0)
        #cv2.imwrite("out.jpg", cobined_image)
        #cv2.imwrite("disparity.jpg", self.color_disparity)
        cv2.destroyAllWindows()

    def visualization_rgb(self):
        rgbd_image=o3d.geometry.RGBDImage.create_from_color_and_depth(o3d.geometry.Image(cv2.cvtColor(self.img_left, cv2.COLOR_BGR2RGB)),
                    o3d.geometry.Image(1054*0.12/self.disparity_map),convert_rgb_to_intensity=False)
        pcd = o3d.geometry.PointCloud.create_from_rgbd_image(
                rgbd_image,
                o3d.camera.PinholeCameraIntrinsic(1920,1080,1054,1054,1920/2,1080/2))
        coord=o3d.geometry.TriangleMesh.create_coordinate_frame(size=0.0005)
        o3d.visualization.draw_geometries([pcd,coord])


def main():
    # init crestereo
    crestereo_model=load_model('./CREStereo/crestereo_eth3d.mge')
    dpe=DepthEstimate(model=crestereo_model,need_to_calibrate=1)
    
    # read img
    img=cv2.cvtColor(cv2.imread('test.jpg'),cv2.COLOR_BGR2RGB)
    dpe.update(img)

    img_left=dpe.img_left
    disparity_map=np.array(dpe.disparity_map)
    visualizer=Visualiz()

    #image = cv2.cvtColor(dpe.img_left,cv2.COLOR_BGR2RGB)/255.
    image = dpe.img_left/255.
    point_colors=image.reshape(-1,3)


    if not visualizer.canvas_inited:
        visualizer.init_canvas()
    img=visualizer.update(
        points=dpe.xyz,
        #points=None,
        ref_boxes=None,
        point_colors=point_colors,
        skeletons=None,
    )
    return img

if __name__ == '__main__':
    main()