from DepthEstimate import DepthEstimate,load_model
from ovsd import Ovsd,show_box,show_mask
import cv2
import numpy as np
import matplotlib.pyplot as plt
from visualiz import Visualiz
from scipy.signal import medfilt
from scipy.spatial import ConvexHull
import open3d as o3d
from tqdm import tqdm
import glob



def _to_o3d_point_cloud(p):
    xyz = np.zeros((p.shape[0], 3))
    x=np.reshape(p[:,0],-1)
    y=np.reshape(p[:,1],-1)
    z=np.reshape(p[:,2],-1)
    xyz[:,0]=x
    xyz[:,1]=y
    xyz[:,2]=z
    return xyz

def _get_bbox3d(point_cloud):
    min_x = np.min(point_cloud[:, 0])
    max_x = np.max(point_cloud[:, 0])
    min_y = np.min(point_cloud[:, 1])
    max_y = np.max(point_cloud[:, 1])
    min_z = np.min(point_cloud[:, 2])
    max_z = np.max(point_cloud[:, 2])

    center_x = (min_x + max_x) / 2.0
    center_y = (min_y + max_y) / 2.0
    center_z = (min_z + max_z) / 2.0

    length = max_x - min_x
    width = max_y - min_y
    height = max_z - min_z

    rotation=0
    box_params = np.array([center_x, center_y, center_z, length, width, height,rotation])
    return box_params

def _get_bbox3d_convexhull(point_cloud):
    hull = ConvexHull(point_cloud)
    vertices = point_cloud[hull.vertices]
    min_coords = np.min(vertices, axis=0)
    max_coords = np.max(vertices, axis=0)
    center = (min_coords + max_coords) / 2.0
    lwh = max_coords - min_coords
# Convert rotation angle to radians
    #rotation_rad = np.radians(rotation)
    box_params = np.array([*center, *lwh, 0])
    return box_params

def _mid_filter(point_cloud,window_size=17,tolerance=0.1):
    z_values = point_cloud[:, 2]
    # Apply median filter to the Z coordinates to smooth and remove outliers
    smoothed_z_values = medfilt(z_values, kernel_size=window_size)
    #print(smoothed_z_values)
    # Select points with Z values within the middle range
    object_point_cloud = point_cloud[
        (z_values >= smoothed_z_values - tolerance) & (z_values <= smoothed_z_values + tolerance)
    ]
    return object_point_cloud

def _center_filter(point_cloud,tolerance=1):
    average_depth = np.mean(point_cloud[:, 2])
    selected_points = point_cloud[np.abs(point_cloud[:, 2] - average_depth) < tolerance]
    return selected_points

def _random_select(point_cloud,alpha=0.1):
    total_points = point_cloud.shape[0]
    random_indices = np.random.choice(total_points, int(total_points*alpha), replace=False)
    randomly_selected_points = point_cloud[random_indices]
    return randomly_selected_points

def voxelization(point_cloud, voxel_size):
    point_cloud_o3d = o3d.geometry.PointCloud()
    point_cloud_o3d.points = o3d.utility.Vector3dVector(point_cloud)
    voxel_grid = o3d.geometry.VoxelGrid.create_from_point_cloud(point_cloud_o3d, voxel_size)
    voxelized_point_cloud_o3d = voxel_grid.sample_points_poisson_disk()
    voxelized_point_cloud_numpy = np.asarray(voxelized_point_cloud_o3d.points)
    return voxelized_point_cloud_numpy

class Detection3d():
    def __init__(self,dpe:DepthEstimate,ovsd:Ovsd):
        self.dpe=dpe
        self.ovsd=ovsd
        self.visualizer=Visualiz()
    
    def update(self,img,prompt):
        self.dpe.update(img)
        img_l=self.dpe.img_left
        boxes_filt,pred_phrases,masks=self.ovsd.update(img_l,prompt)

        #plt.figure(figsize=(10, 10))
        #plt.imshow(img)
        #for mask in masks:
        #    show_mask(mask.cpu().numpy(), plt.gca(), random_color=True)
        #for box, label in zip(boxes_filt, pred_phrases):
        #    show_box(box.numpy(), plt.gca(), label)
        #plt.savefig(
        #    "grounded_sam_output.jpg", 
        #    bbox_inches="tight", dpi=300, pad_inches=0.0
        #)

        bboxes=[]
        cln_list=[]
        pcd=self.dpe.img_3d
        world_pcd=_to_o3d_point_cloud(pcd.reshape(pcd.shape[0]*pcd.shape[1],3))

        image = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)/255.
        point_colors=image.reshape(-1,3)

        for mask,cln in zip(masks,pred_phrases):
            cmask=mask.squeeze(0).cpu().numpy()
            obj_pcd=pcd[cmask,:]
            obj_pcd=_to_o3d_point_cloud(obj_pcd)

            # midfilter
            point_cloud=obj_pcd
            #point_cloud=voxelization(point_cloud,10)
            #point_cloud=_random_select(point_cloud)

            object_point_cloud=_mid_filter(point_cloud)
            object_point_cloud=_center_filter(object_point_cloud)

            # center distance
            #box=_get_bbox3d_convexhull(object_point_cloud)
            box=_get_bbox3d(object_point_cloud)
            #print(box)
            bboxes.append(box)
            cln_list.append(cln)
        self.bboxes_3d=bboxes
        self.cln_list=cln_list
        print(cln_list)
        return bboxes,cln_list

    def visualization(self):
        # Open3D Image use (0-1) as color range
        image = cv2.cvtColor(self.dpe.img_left,cv2.COLOR_BGR2RGB)/255.
        point_colors=image.reshape(-1,3)

        boxes=self.bboxes_3d
        #skeletons=self.skeletons_3d
        skeletons=None

        if not self.visualizer.canvas_inited:
            self.visualizer.init_canvas()
        img=self.visualizer.update(
            points=self.dpe.xyz,
            #points=None,
            ref_boxes=np.array(boxes),
            point_colors=point_colors,
        )
        return img

def main():
    ovsd=Ovsd()
    crestereo_model=load_model('./CREStereo/crestereo_eth3d.mge')
    dpe=DepthEstimate(model=crestereo_model,need_to_calibrate=1)
    det=Detection3d(dpe=dpe,ovsd=ovsd)
    img=cv2.imread('./test.jpg')
    prompt='person.white board.'
    det.update(img,prompt=prompt)
    det.visualization()



if __name__=='__main__':
    main()
    #test_kitti()